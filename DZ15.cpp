﻿#include <iostream>

const int n = 16;

void EvenNumbers()
{
	std::cout << "Even Numbers: ";
	for (int a = 0; a <= n; ++a)
	{
		switch (a % 2)
		{
		case 0:
			std::cout << a << " ";
		}
	}
}

void OddNumbers()
{
	std::cout << "Odd Numbers: ";
	for (int a = 0; a <= n; ++a)
	{
		switch (a % 2)
		{
		case 1:
			std::cout << a << " ";
		}
	}
}

int main()
{
	EvenNumbers();
	std::cout << '\n';
	OddNumbers();
}
